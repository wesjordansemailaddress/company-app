ARG RUST_IMG=rust:1.52

# Dockerfile based on https://www.lpalmieri.com/posts/fast-rust-docker-builds/

FROM $RUST_IMG as planner
WORKDIR app
# We only pay the installation cost once, 
# it will be cached from the second build onwards
RUN cargo install cargo-chef --version 0.1.21
COPY companyapp .
RUN cargo chef prepare  --recipe-path recipe.json

FROM $RUST_IMG as cacher
WORKDIR app
RUN cargo install cargo-chef --version 0.1.21
COPY --from=planner /app/recipe.json recipe.json
RUN cargo chef cook --release --recipe-path recipe.json

FROM $RUST_IMG as builder
WORKDIR app
COPY companyapp .
# Copy over the cached dependencies
COPY --from=cacher /app/target target
COPY --from=cacher /usr/local/cargo /usr/local/cargo
RUN cargo build --release --bin app

FROM $RUST_IMG as runtime
WORKDIR app
COPY --from=builder /app/target/release/app /usr/local/bin
ENTRYPOINT ["/usr/local/bin/app"]