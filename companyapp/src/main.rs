fn main() {
    trillium_smol::config()
        .with_port(8080)
        .with_host("0.0.0.0")
        .run(|conn: trillium::Conn| async move {
            conn.ok("hello from companyapp!")
        });
}
