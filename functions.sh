export APP_NAME=companyapp
export GIT_REPO=wesjordansemailaddress/company-app
export VERSION=0.1.0
export BRANCH=$(git symbolic-ref --short -q HEAD)
export DOCKER_IMAGE=$GIT_REPO:$VERSION-$BRANCH
export NAMESPACE=$(echo $GIT_REPO/$BRANCH | sed "s/\//--/g")

set -eux

function companyapp-docker-build() {
  docker build . -t $DOCKER_IMAGE
}

function companyapp-docker-run() {
  docker run --rm -p 8080:8080 $DOCKER_IMAGE
}

function companyapp-docker-shell() {
  docker run --rm -p 8080:8080 -it --entrypoint sh $DOCKER_IMAGE
}

function companyapp-deploy() {
  helm -n $NAMESPACE install $APP_NAME helm $argv
}

function companyapp-portforward() {
  kubectl -n $NAMESPACE port-forward service/$APP_NAME 8080:80
}

function companyapp-preview-env() {
  cat argocd/app-template.yml | envsubst | tee| kubectl -n argocd apply -f -
}