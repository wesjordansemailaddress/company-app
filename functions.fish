set -x APP_NAME companyapp
set -x GIT_REPO wesjordansemailaddress/company-app
set -x VERSION 0.1.0
set -x BRANCH (git symbolic-ref --short -q HEAD)
set -x DOCKER_IMAGE $GIT_REPO:$VERSION-$BRANCH
set -x NAMESPACE (echo $GIT_REPO/$BRANCH | sed "s/\//--/g")

function companyapp-docker-build
  docker build . -t $DOCKER_IMAGE
end

function companyapp-docker-run
  docker run --rm -p 8080:8080 $DOCKER_IMAGE
end

function companyapp-docker-shell
  docker run --rm -p 8080:8080 -it --entrypoint sh $DOCKER_IMAGE
end

function companyapp-deploy
  helm -n $NAMESPACE install $APP_NAME helm $argv
end

function companyapp-portforward
  kubectl -n $NAMESPACE port-forward service/$APP_NAME 8080:80
end